(function(){

	const hamburgerBtn = document.getElementById('hamburger-btn');
	const stickOne = document.getElementById('stick-one');
	const stickTwo = document.getElementById('stick-two');
	const stickThree = document.getElementById('stick-three');
	let active = false;

	const stickObject = {

		one:{
			name: stickOne,
			addAnimationEffect: 'start-animation-stick-one',
			removeAnimationEffect: 'remove-animation-stick-one'
		},

		two:{
			name: stickTwo,
			addAnimationEffect: 'start-animation-stick-two',
			removeAnimationEffect: 'remove-animation-stick-two'
		},

		three:{
			name: stickThree,
			addAnimationEffect: 'start-animation-stick-three',
			removeAnimationEffect: 'remove-animation-stick-three'
		}

	};


	

	const animateButton = function(){

		for(let i in stickObject){

			if(!stickObject[i].name.classList.contains(stickObject[i].addAnimationEffect)){

				stickObject[i].name.classList.add(stickObject[i].addAnimationEffect);
				stickObject[i].name.classList.remove(stickObject[i].removeAnimationEffect);
				active = true;

			}else{

				stickObject[i].name.classList.add(stickObject[i].removeAnimationEffect);
				stickObject[i].name.classList.remove(stickObject[i].addAnimationEffect);
				active = false
			};
			
		};
			
	};

	
	const closeButton = function(event){

		if(event.keyCode == 27 && active == true){
			animateButton();
		};
		
	};



	hamburgerBtn.addEventListener('click', animateButton);
	document.addEventListener('keyup', closeButton);

})();